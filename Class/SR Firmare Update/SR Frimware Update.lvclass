﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="NI.Lib.ContainingLib" Type="Str">Test Item.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../../Test Item.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91P&lt;!FNA#^M#5Y6M96NA"R[WM#WQ"&lt;9A0ZYR'E?G!WPM1$AN&gt;@S(!ZZQG&amp;0%VLZ'@)H8:_X\&lt;^P(^7@8H\4Y;"`NX\;8JZPUX@@MJXC]C.3I6K5S(F/^DHTE)R`ZS%@?]J;XP/5N&lt;XH*3V\SEJ?]Z#F0?=J4HP+5&lt;Y=]Z#%0/&gt;+9@%QU"BU$D-YI-4[':XC':XB]D?%:HO%:HO(2*9:H?):H?)&lt;(&lt;4%]QT-]QT-]BNIEMRVSHO%R@$20]T20]T30+;.Z'K".VA:OAW"%O^B/GK&gt;ZGM&gt;J.%`T.%`T.)`,U4T.UTT.UTROW6;F.]XDE0-9*IKH?)KH?)L(U&amp;%]R6-]R6-]JIPC+:[#+"/7Q2'CX&amp;1[F#`&amp;5TR_2@%54`%54`'YN$WBWF&lt;GI8E==J\E3:\E3:\E-51E4`)E4`)EDW%D?:)H?:)H?5Q6S:-]S:-A;6,42RIMX:A[J3"Z`'S\*&lt;?HV*MENS.C&lt;&gt;Z9GT,7:IOVC7*NDFA00&gt;&lt;$D0719CV_L%7.N6CR&amp;C(7(R=,(1M4;Z*9.T][RNXH46X62:X632X61?X6\H(L8_ZYP^`D&gt;LP&amp;^8K.S_53Z`-Z4K&gt;4()`(/"Q/M&gt;`P9\@&lt;P&lt;U'PDH?8AA`XUMPTP_EXOF`[8`Q&lt;IT0]?OYVOA(5/(_Z!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.1</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!")&lt;5F.31QU+!!.-6E.$4%*76Q!!$&gt;Q!!!1`!!!!)!!!$&lt;Q!!!!P!!!!!A^5:8.U)%FU:7UO&lt;(:M;7);5V)A2H*J&lt;8&gt;B=G5A68"E982F,GRW9WRB=X-!!!!!I"=!A!!!-!!!+!!!!!!!!!1!!Q!]!,Q!(U!!!A!!!!!"!!%!"P````]!!!!!!!!!!!!!!!#.1W,5&gt;7H#2KB!4!QA[9Z\!!!!$!!!!"!!!!!"%B\8&lt;&amp;@6IU36@F,)&amp;37['.1&gt;D.G0!,)%[9!*G/TY1HY!!"!!!!!!!)Z!L5JR\2*.C]_A`[R:FR1"!!!!`````^1&gt;D.G0!,)%[9!*G/TY1HY!!!!1ZW-)U)N_K3&amp;=VNIKU72=;1!!!!1!!!!!!!!!*Q!"4&amp;:$1Q!!!!%!!F:*4%)!!!!!5&amp;2)-!!!!!5!!1!"!!!!!!)!!Q!!!!!#!!%!!!!!!#)!!!!=?*RD9'.A;G#YQ!$%D%$-V-$U!]D_!/)T!!"I!1AW!!!!!!"&amp;!!!"'(C=9W$!"0_"!%AR-D!QH1$3,'DC9"L'JC&lt;!:3YOO[$CT&amp;!XME*%'9&amp;C4(O!$#;1(+J?E(`!@E)"&lt;&amp;D-"A"W^3A6!!!!!!!!$!!"6EF%5Q!!!!!!!Q!!!71!!!,-?*R,9'2AS$3W-.M!J*G"7*SBA3%Z0S76CQ():Y#!,5Q-&amp;)-!K(F;;/+'"Q[H!9%?PXQ,G.`]BK@&lt;257AO5:&amp;AKF5J.N(2;442Y7FEU8FR:````]X(_%ZX/W2=^T2"K3WGQ-I@NR&amp;B10%!&gt;)M)0J`9!:)&amp;=S]!+"J(!U6SAQF,)9(IAY@&lt;T"BB&amp;A--T)+V@Y&gt;)/H$S/\B!$J#Y/"$FOZ'$3#`&gt;S+)"!LR&gt;):Q3"RXY&gt;!2!`):4X1#L?`EA&lt;G3!WZ`'-C!%B7"4B/13VF!JI06&gt;,-&gt;&gt;^!!O^N""%*F1+A+#&amp;5!&gt;AT9"5=YYA\$QWPN[XO\1/()BB3'$E$=!-3A/%4'?AS-$#!,G9"E,63N$:$.""7$R17)@1(+VE$3]Q8*@*!?E-Q;K"C)P1H+&lt;I#["S4W&amp;UB0A,*"PEW!MLG"\!61NB#1,1"F3Q,:$["M/3B\!T3+=.(/`C[O3.[(JU]!.2VSA!!!!!Y8!9!3!!!'-4=O-#YR!!!!!!!!$"=!A!!!!!1R.SYQ!!!!!!Y8!9!3!!!'-4=O-#YR!!!!!!!!$"=!A!!!!!1R.SYQ!!!!!!Y8!9!3!!!'-4=O-#YR!!!!!!!!&amp;!%!!!$V6T7#?3;CD#ZT5EY'34G&gt;!!!!$1!!!!!!!!!!!!!!!!!!!!!!!!#!`````Y!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A'!!!9'9!!''"A!"G!'!!;!!1!'Q!-!"L!.!!;-/Q!'A^5!"I#L!!;!V1!'A+M!"I$6!!;!KQ!'A.5!"G#O!!99W!!'"O!!"A'!!!@````]!!!1!````````````````````````````````````````````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!O&lt;E!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!O&gt;(&amp;S^'Z!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!O&gt;(&amp;P\_`P]P2O1!!!!!!!!!!!!!!!!!!!!$``Q!!O&gt;(&amp;P\_`P\_`P\`,U&lt;E!!!!!!!!!!!!!!!!!!0``!-P&amp;P\_`P\_`P\_`P\_`S^%!!!!!!!!!!!!!!!!!``]!R=7`P\_`P\_`P\_`P\``SQ!!!!!!!!!!!!!!!!$``Q$&amp;S]P&amp;P\_`P\_`P\`````&amp;!!!!!!!!!!!!!!!!!0``!-8,S]P,R&lt;_`P\```````]5!!!!!!!!!!!!!!!!!``]!R=P,S]P,S]82````````R1!!!!!!!!!!!!!!!!$``Q$&amp;S]P,S]P,S``````````&amp;!!!!!!!!!!!!!!!!!0``!-8,S]P,S]P,`````````]5!!!!!!!!!!!!!!!!!``]!R=P,S]P,S]P`````````R1!!!!!!!!!!!!!!!!$``Q$&amp;S]P,S]P,S``````````&amp;!!!!!!!!!!!!!!!!!0``!-8,S]P,S]P,`````````]5!!!!!!!!!!!!!!!!!``]!S]P,S]P,S]P````````,SQ!!!!!!!!!!!!!!!!$``Q!!R=8,S]P,S``````,U=5!!!!!!!!!!!!!!!!!!0``!!!!!-8,S]P,```,S]5!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!$&amp;S]P,S\]!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!R&lt;]!!!!!!!!!!!!!!!!!!!!!!!!!!0```````````````````````````````````````````Q!!!!)!!1!!!!!!$!!"2F")5!!!!!!!!Q!!!GE!!!4C?*SNF-^L%U%5R^_%63;BR5E;.1MNC753CS18@^:AN,441[55*2:[+.DARB`1'GH3WF.\791=#A5B"[%(,\F[S-&amp;LE?"F$XLS9-'F_1O]&amp;!7\C7]HW&gt;UE9LS9Q$!E\`/_&lt;^`XSQ)IXVD%VY2N#QA\RMO]"1(.)!$V&amp;)8/*`%+W#,Z"33M%AOG[#)\^$8*K!6$GJ'A6U5:PH?K39;]:5&gt;9?JKJW#RAQ2H./"_]SUX'XYXS]CGH;RDC&lt;*=U@5M]_I0O[A55"(X#0I-JUA1C,CC+0HYP^TSP=`N8@YKKMK8@!C;-Y5VO8M3/+0V"NC2\*)H3H:;!,3?A6KNZ5,!.*?19E]C1J"RX&lt;Q!41O9F.Z/3#5A'&gt;4+/DIAW8^OTWV!`/M+.)D=2WX)WEX3E2.3;EFCDU5!-TQ\WV))Q.[?J3I_ULW4YPPY?#*$[1^?(Y)*NAKQ.I1`R.'&amp;:P'=NO+1&lt;PB7G/.P&gt;AA0JAO+Y=-NW96K[Y"&gt;F"JY."`_UI4Y_.L/[53TFVW/&amp;R\&amp;(K\FC-@:C`&gt;FGLJ30;&lt;F3\E_$-M*1L^B0&lt;YOVMR%""5KQULXM!F3L66Q!HBZ['^%2&lt;LJ=7$[.ZCZ=]R:OKXK&lt;OY/&lt;%T]@0,'X&amp;U`\H-C#&amp;^&lt;,'.:AO(_AHL"?_`^BP3[-M?W_M-)S@N='"/^''`,#CP5!;\!]A*F%:K=\L'U62W&gt;17']+A`7GV7&lt;\UFKJ6(IYX(H;4;N#3$ON_O@73?OE0\0A8_C;H"Z_X($`JVEO7F4^?Q'&lt;2&lt;%&gt;#][+G&gt;!H.I@&gt;BOA=H78\[#K_D=\2@8J=`_+7V_@&gt;+X]4J:(@\-*8&amp;A!!!!!!!!1!!!!A!!!!"!!!!!!!!!!-!!&amp;#2%B1!!!!!!!$!!!!9A!!!(*YH'.A9-A4E'$[RV$XFY&amp;*Y#O1)@W8A6H1D`%X!Q/HH]"B)-UI)!E5FPX,Q#[I$2&lt;70K,,Q1!&amp;KGS-(*)=BQ5ZQ$)=,2I-````Z`B[Z"J=R2%@/&amp;.FFDS("!!59BE!!!!!!!!%!!!!"Q!!!,I!!!!#!!!!)6^O;6^-98.U3WZP&gt;WZ0&gt;WZJ&lt;G&gt;-6E.M98.T1WRV=X2F=A!!!%98!)!!!!!!!1!)!$$`````!!%!!!!!!#I!!!!"!#*!5!!!'F.3)%:S;7VX98*F)&amp;6Q:'&amp;U:3ZM&gt;G.M98.T!!!"!!!!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962B9E^S:'6S!!!!*2=!A!!!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!!!!!!!!!!!!!!!1!!A!)!!!!"!!!!%!!!!!I!!!!!A!!"!!!!!!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!/U!!!&amp;V?*S.D]V+!U%1B,`.20/H*CJY%)2"0(DSYAMM#!&amp;P)?L&gt;.4ML#R-WT%[C2R`5B^!XM,+*?.#$&amp;$44V&gt;660=!RA=`XKDQ(T0$?V&gt;(?2D?`]CN@0JX?4?UYF0/8,$D\M-CT[$39_;SO4`Y9T;,H-JX!W9V@VN%&amp;7R7W5&gt;N&amp;+&amp;?37/ES&gt;A4[Z*"]E+ARWZ:L08NJ"V0Y:\LJG[GG=5/YKG!E&lt;S0Z,BWTT!P[[SQ48X.N4WD2J=@&amp;P`*&lt;.$9K#6J\6(3&lt;M5QM?_S,8/.!9&gt;^)NPD.`%S'$49@32D*%2W%QNJC"RRSJ$LY!ET'1%Y!!!!!!!#-!!%!!A!$!!1!!!")!!]!!!!!!!]!\1$D!!!!8A!0!!!!!!!0!/U!YQ!!!(1!$Q!!!!!!$Q$N!/-!!!#+!")!!!0I!")"(1%1&amp;5VJ9X*P=W^G&gt;#"+;'6O:UBF;3"6326.;7.S&lt;X.P:H1A3GBF&lt;G&gt;):7EA65E647FD=G^T&lt;W:U)%JI:7ZH3'6J)&amp;6*!4"35V*$$1I!!UR71U.-1F:8!!!.X!!!"$]!!!!A!!!.P!!!!!!!!!!!!!!!)!!!!$1!!!1I!!!!(%R*1EY!!!!!!!!"9%R75V)!!!!!!!!"&gt;&amp;*55U=!!!!!!!!"C%.$5V1!!!!!!!!"H%R*&gt;GE!!!!!!!!"M%.04F!!!!!!!!!"R&amp;2./$!!!!!!!!!"W%2'2&amp;-!!!!!!!!"\%R*:(-!!!!!!!!#!&amp;:*1U1!!!!!!!!#&amp;(:F=H-!!!!%!!!#+&amp;.$5V)!!!!!!!!#D%&gt;$5&amp;)!!!!!!!!#I%F$4UY!!!!!!!!#N'FD&lt;$A!!!!!!!!#S%.11T)!!!!!!!!#X%R*:H!!!!!!!!!#]%:13')!!!!!!!!$"%:15U5!!!!!!!!$'&amp;:12&amp;!!!!!!!!!$,%R*9G1!!!!!!!!$1%*%3')!!!!!!!!$6%*%5U5!!!!!!!!$;&amp;:*6&amp;-!!!!!!!!$@%253&amp;!!!!!!!!!$E%V6351!!!!!!!!$J%B*5V1!!!!!!!!$O&amp;:$6&amp;!!!!!!!!!$T%:515)!!!!!!!!$Y!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!.!!!!!!!!!!!`````Q!!!!!!!!$9!!!!!!!!!!$`````!!!!!!!!!/Q!!!!!!!!!!0````]!!!!!!!!!^!!!!!!!!!!!`````Q!!!!!!!!%A!!!!!!!!!!$`````!!!!!!!!!3A!!!!!!!!!!0````]!!!!!!!!"5!!!!!!!!!!!`````Q!!!!!!!!'=!!!!!!!!!!$`````!!!!!!!!!;Q!!!!!!!!!"0````]!!!!!!!!$&amp;!!!!!!!!!!(`````Q!!!!!!!!-I!!!!!!!!!!D`````!!!!!!!!!TA!!!!!!!!!#@````]!!!!!!!!$4!!!!!!!!!!+`````Q!!!!!!!!.=!!!!!!!!!!$`````!!!!!!!!!X!!!!!!!!!!!0````]!!!!!!!!$C!!!!!!!!!!!`````Q!!!!!!!!/=!!!!!!!!!!$`````!!!!!!!!"#!!!!!!!!!!!0````]!!!!!!!!)*!!!!!!!!!!!`````Q!!!!!!!!AM!!!!!!!!!!$`````!!!!!!!!#$Q!!!!!!!!!!0````]!!!!!!!!+L!!!!!!!!!!!`````Q!!!!!!!!KU!!!!!!!!!!$`````!!!!!!!!#LQ!!!!!!!!!!0````]!!!!!!!!+T!!!!!!!!!!!`````Q!!!!!!!!MU!!!!!!!!!!$`````!!!!!!!!#TQ!!!!!!!!!!0````]!!!!!!!!,`!!!!!!!!!!!`````Q!!!!!!!!Q%!!!!!!!!!!$`````!!!!!!!!$!Q!!!!!!!!!!0````]!!!!!!!!-/!!!!!!!!!#!`````Q!!!!!!!!UM!!!!!":45C"'=GFN&gt;W&amp;S:3"6='2B&gt;'5O9X2M!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>
<Name></Name>
<Val>!!!!!A^5:8.U)%FU:7UO&lt;(:M;7);5V)A2H*J&lt;8&gt;B=G5A68"E982F,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!!!!!A!"!!!!!!!!!!!!!!%!)E"1!!!;5V)A2H*J&lt;8&gt;B=G5A68"E982F,GRW9WRB=X-!!!%!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!,``Q!!!!%!!!!!!!%!!!!!!1!C1&amp;!!!"J45C"'=GFN&gt;W&amp;S:3"6='2B&gt;'5O&lt;(:D&lt;'&amp;T=Q!!!1!!!!!!!@````Y!!!!!!A^5:8.U)%FU:7UO&lt;(:M;7)26'6T&gt;#"*&gt;'6N,GRW9WRB=X.16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!!!!!!</Val>
</String>
</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.1</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"(!!!!!A^5:8.U)%FU:7UO&lt;(:M;7)26'6T&gt;#"*&gt;'6N,GRW9WRB=X.16%AQ!!!!'1!"!!1!!!!26'6T&gt;#"*&gt;'6N,GRW9WRB=X-!!!!!</Property>
	<Item Name="SR Frimware Update.ctl" Type="Class Private Data" URL="SR Frimware Update.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Override" Type="Folder">
		<Item Name="Do.vi" Type="VI" URL="../Do.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%Z!=!!?!!!M$V2F=X1A382F&lt;3ZM&gt;GRJ9BJ45C"'=GFN&gt;W&amp;S:3"6='2B&gt;'5O&lt;(:D&lt;'&amp;T=Q!!&amp;F.3)%:S;7VX98*F)&amp;6Q:'&amp;U:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!4%"Q!"Y!!#Q06'6T&gt;#"*&gt;'6N,GRW&lt;'FC'F.3)%:S;7VX98*F)&amp;6Q:'&amp;U:3ZM&gt;G.M98.T!!!65V)A2H*J&lt;8&gt;B=G5A68"E982F)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
	</Item>
</LVClass>
